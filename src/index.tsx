import React from "react";
import ReactDOM from "react-dom";

import { StickyNotesPane } from "./components/sticky-notes/pane/sticky-notes-pane";

import "./index.css";

ReactDOM.render(
  <React.StrictMode>
    <StickyNotesPane />
  </React.StrictMode>,
  document.getElementById("root")
);
