import { getRandomHexColorString } from "../../core/colors";
import { DraggableItem } from "../../core/draggable-manager";
import { Point } from "../../core/point";

export interface StickyNoteModel extends DraggableItem {
    id: number;
    layerIndex: number;
    width: number
    height: number;
    position: Point;
    hexBackgroundColor: string;
    text: string;
}

export class StickyNoteItem implements StickyNoteModel {
    constructor(
        public id: number,
        public layerIndex: number,
        public position: Point,
        public text = "Text example",
        public width: number = 100,
        public height: number = 100,
        public hexBackgroundColor = getRandomHexColorString(),
        ) {
    }

    public static fromModel(model: StickyNoteModel): StickyNoteItem {
        return new StickyNoteItem(
            model.id,
            model.layerIndex,
            model.position,
            model.text,
            model.width,
            model.height,
            model.hexBackgroundColor
        )
    }
}
