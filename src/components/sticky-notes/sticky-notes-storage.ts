import { StickyNoteItem, StickyNoteModel } from "./sticky-note-model";

export interface StickyNotesStorage {
    getAll(): Promise<Map<number, StickyNoteModel>>;
    addOrUpdate(item: StickyNoteModel): Promise<void>;
    remove(item: StickyNoteModel): Promise<void>;
}

export class StickyNotesLocalStorage implements StickyNotesStorage {
    private static stickyNotesKey = "sticky_notes"

    public async getAll(): Promise<Map<number, StickyNoteModel>> {
        const storedValue = localStorage.getItem(StickyNotesLocalStorage.stickyNotesKey);
        if (storedValue) {
            return Promise.resolve(new Map(JSON.parse(storedValue)));
        }
        return Promise.resolve(new Map());
    }

    public async addOrUpdate(item: StickyNoteModel): Promise<void> {
        const notes = await this.getAll();
        notes.set(item.id, item);
        this.saveItemsToLocalStorage(notes);
    }

    public async remove(item: StickyNoteModel): Promise<void> {
        const notes = await this.getAll();
        notes.delete(item.id);
        this.saveItemsToLocalStorage(notes);
    }

    private saveItemsToLocalStorage(items: Map<number, StickyNoteModel>) {
        localStorage.setItem(StickyNotesLocalStorage.stickyNotesKey, JSON.stringify(Array.from(items.entries())));
    }
}

export class StickyNotesInMemoryStorage implements StickyNotesStorage {
    private items = new Map<number, StickyNoteItem>();

    public getAll(): Promise<Map<number, StickyNoteItem>> {
        return Promise.resolve(this.items);
    }

    public addOrUpdate(item: StickyNoteModel): Promise<void> {
        this.items.set(item.id, item);
        return Promise.resolve();
    }

    public remove(item: StickyNoteModel): Promise<void> {
        this.items.delete(item.id);
        return Promise.resolve();
    }
}