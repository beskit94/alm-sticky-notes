import { Component } from "react";
import * as _ from "underscore";

import { DraggableManager } from "../../../core/draggable-manager";
import { Point } from "../../../core/point";
import StickyNote from "../note/sticky-note";
import { StickyNoteItem } from "../sticky-note-model";
import { StickyNotesLocalStorage, StickyNotesStorage } from "../sticky-notes-storage";

import "./sticky-notes-pane.css";

export class StickyNotesPane extends Component<{}, { notes: ReadonlyArray<StickyNoteItem>, loadingInProgress: boolean }> {
    public render(): JSX.Element {
        return (
        <div className="alm-sticky-notes-pane" onClick={e => this.onPaneClick(e)}>
            <button onClick={event => this.onCreateButtonClick(event)} disabled={this.state.loadingInProgress}>Create</button>
                {this.notes.map(note => {
                    const noteClasses = ["alm-sticky-note-container"];
                    if (this.draggableManager.draggedItem === note) {
                        noteClasses.push("alm-sticky-note-draggable");
                    }
                    return <div draggable="true"
                                onClick={e => this.draggableManager.moveItemToTop(this.notes, note)}
                                onDragStart={e => this.draggableManager.startDrag(note, { x: e.pageX, y: e.pageY })}
                                onDragExit={e => this.draggableManager.endDrag(note, { x: e.pageX, y: e.pageY })}
                                onDragEnd={e => this.draggableManager.endDrag(note, { x: e.pageX, y: e.pageY })}
                                onDrop={console.log}
                                style={this.getNoteStyle(note)}
                                className={noteClasses.join(" ")}
                                key={note.layerIndex}>
                                    <button className="alm-sticky-note-remove-button" onClick={() => this.removeItem(note)}>X</button>
                                    <StickyNote text={note.text}
                                                width={note.width}
                                                height={note.height}
                                                backgroundColor={note.hexBackgroundColor}
                                                onTextChanged={e => this.onNoteTextChanged(note, e)}
                                                onResized={(width, height) => this.onNoteResized(note, width, height)}/>
                            </div>;
                })}
        </div>)
    }

    public readonly draggableManager: DraggableManager<StickyNoteItem>;
    private readonly storage: StickyNotesStorage = new StickyNotesLocalStorage();

    public isCreateModeActive = false;
    public newItemStartPoint: Point | null = null;

    public readonly saveItemDebounced: ((note: StickyNoteItem) => void) & _.Cancelable;

    constructor(props: {}) {
        super(props);

        this.saveItemDebounced = _.debounce<(note: StickyNoteItem) => void>(
            note => {this.storage.addOrUpdate(note)}, 500)

        this.draggableManager = new DraggableManager<StickyNoteItem>();

        this.draggableManager.itemDragStart.subscribe(i => this.forceUpdate());
        this.draggableManager.itemDragEnd.subscribe(i => {
            this.draggableManager.moveItemToTop(this.notes, i);
            this.saveItemDebounced(i);
            this.forceUpdate();
        });
        this.draggableManager.movedToFront.subscribe(i => {
            this.saveItemDebounced(i);
            this.forceUpdate();
        });

        this.state = {
            notes: [],
            loadingInProgress: true
        };
    }

    public async componentDidMount() {
        const models = await this.storage.getAll();
        this.setState({
            notes: Array.from(models).map(([id, model]) => StickyNoteItem.fromModel(model)),
            loadingInProgress: false
        });
    }

    public get notes(): ReadonlyArray<StickyNoteItem> {
        return this.state.notes;
    }

    public onPaneClick(event: React.MouseEvent) {
        if (this.state.loadingInProgress || !this.isCreateModeActive) {
            return;
        }
        if (!this.newItemStartPoint) {
            this.newItemStartPoint = { x: event.pageX, y: event.pageY };
            return;
        }
        const minX = Math.min(this.newItemStartPoint.x, event.pageX);
        const minY = Math.min(this.newItemStartPoint.y, event.pageY);
        const maxX = Math.max(this.newItemStartPoint.x, event.pageX);
        const maxY = Math.max(this.newItemStartPoint.y, event.pageY);
        const width = maxX - minX;
        const height = maxY - minY;

        this.isCreateModeActive = false;
        this.newItemStartPoint = null;
        this.addNote({ x: minX, y: minY }, width, height);
    }

    public onCreateButtonClick(event: React.MouseEvent) {
        this.isCreateModeActive = true;
        event.stopPropagation();
    }

    public onNoteTextChanged(note: StickyNoteItem, text: string) {
        note.text = text;
        this.saveItemDebounced(note);
        this.forceUpdate();
    }

    private onNoteResized(note: StickyNoteItem, width: number, height: number) {
        note.width = width;
        note.height = height;
        this.saveItemDebounced(note);
        this.forceUpdate();
    }

    public removeItem(note: StickyNoteItem) {
        const notes = this.notes.filter(i => i !== note);
        this.storage.remove(note);
        this.setState({
            notes: notes,
            loadingInProgress: false
        });
    }

    public getNoteStyle(note: StickyNoteItem): React.CSSProperties {
        return {
            position: "absolute",
            top: note.position.y,
            left: note.position.x,
            zIndex: note.layerIndex
        };
    }

    private addNote(position: Point, width: number, height: number) {
        const id = this.notes.length > 0
            ? Math.max(...this.notes.map(i => i.id)) + 1
            : 0;
        const layerId = DraggableManager.getTopItemLayerId(this.notes) + 1;
        const item = new StickyNoteItem(id, layerId, position, undefined, width, height);
        this.saveItemDebounced(item);
        const notes = this.notes.concat([item]);
        this.setState({
            notes: notes,
            loadingInProgress: false
        })
    }

    public componentWillUnmount() {
        this.draggableManager.destroy();
    }
}
