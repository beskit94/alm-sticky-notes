import React from "react";
import $ from "jquery";

import "./sticky-note.css";

class StickyNote extends React.PureComponent<
    {
        text: string,
        height: number,
        width: number,
        backgroundColor: string,
        onTextChanged: (text: string) => void,
        onResized: (width: number, height: number) => void
    }> {

    public render() {
        return (
            <textarea className="alm-sticky-note"
                      ref={this.inputRef}
                      value={this.props.text}
                      style={{
                          backgroundColor: this.props.backgroundColor,
                          height: this.props.height,
                          width: this.props.width
                      }}
                      onChange={e => this.props.onTextChanged(e.target.value)}/>);
    }

    public readonly inputRef = React.createRef<HTMLTextAreaElement>();

    public componentDidMount() {
        if (this.inputRef?.current == null) {
            return;
        }
        $(this.inputRef.current).on("mouseup", (e) => {
            const elem = $(e.target);
            const width = elem.width();
            const height = elem.height();
            if (this.props.width !== width || this.props.height !== height) {
                if (width && height) {
                    this.props.onResized(width, height);
                }
            }
        });
    }
}

export default StickyNote;
