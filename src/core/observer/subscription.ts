import { Observer } from "./observer";

export class Subscription<T> {
    constructor(
        private readonly observer: Observer<T>,
        public readonly callback: (value: T) => void) {}

    public unsubscribe() {
        this.observer.unsubscribe(this);
    }
}
