import { Subscription } from "./subscription";

export class Observer<T> {
    private isComplete = false;
    private subscriptions: Subscription<T>[] = [];

    public next(value: T) {
        this.errorIfComplete();
        this.subscriptions.forEach(i => i.callback(value));
    }

    public subscribe(callback: (value: T) => void): Subscription<T> {
        this.errorIfComplete();
        const subscription = new Subscription(this, callback);
        this.subscriptions.push(subscription);
        return subscription;
    }

    public unsubscribe(subscription: Subscription<T>) {
        this.subscriptions = this.subscriptions.filter(s => s !== subscription)
    }

    public complete() {
        this.isComplete = true;
        this.subscriptions = [];
    }

    private errorIfComplete() {
        if (this.isComplete) {
            throw Error("Observer already complete");
        }
    }
}
