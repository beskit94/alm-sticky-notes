import { Observer } from "./observer/observer";
import { Point } from "./point";

export interface DraggableItem {
    position: Point;
    layerIndex: number
}

export class DraggableManager<T extends DraggableItem> {
    public itemDragStart = new Observer<T>();
    public itemDragEnd = new Observer<T>();
    public movedToFront = new Observer<T>();

    private activeDrag: { item: T, startPosition: Point} | null = null;

    public static getTopItemLayerId(items: ReadonlyArray<DraggableItem>): number {
        return items.reduce((acc, value) => {
            if (value.layerIndex > acc) {
                return value.layerIndex;
            }
            return acc;
        }, 0);
    }

    public get draggedItem(): T | undefined {
        return this.activeDrag?.item;
    }

    public startDrag(item: T, position: Point) {
        this.activeDrag = { item, startPosition: position };
        this.itemDragStart.next(item);
    }

    public endDrag(item: T, position: Point) {
        const startDragPosition = this.activeDrag?.startPosition;
        this.activeDrag = null;

        if (!startDragPosition) {
            console.warn("End drag event received before drag start.")
            return;
        }

        // [artems] we need to avoid drop item out of page
        const targetX = Math.max(position.x, 0);
        const targetY = Math.max(position.y, 0);

        const offsetX = targetX - startDragPosition.x;
        const offsetY = targetY - startDragPosition.y;
        item.position.x += offsetX;
        item.position.y += offsetY;
        this.itemDragEnd.next(item);
    }

    public moveItemToTop(items: ReadonlyArray<T>, item: T) {
        const frontLayerId = DraggableManager.getTopItemLayerId(items);
        if (item.layerIndex !== frontLayerId) {
            item.layerIndex = frontLayerId + 1;
            this.movedToFront.next(item);
        }
    }

    public destroy() {
        this.activeDrag = null;
        this.itemDragStart.complete();
        this.itemDragEnd.complete();
        this.movedToFront.complete();
    }
}